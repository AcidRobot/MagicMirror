var express = require('express');
var app = express();
var port = 80;
var io = require('socket.io').listen(app.listen(port));

app.use('/static', express.static('public'));

app.get('/', function(req, res)
{
    var options = {
      root: __dirname + '/',
      dotfiles: 'allow',
      headers: {
          'x-timestamp': Date.now(),
          'x-sent': true
      }
    };

    res.sendFile('/index.html', options, function(err)
    {
       if (err)
       {
         console.log(err);
         return res.end('Error loading index.html');
       }
       else
       {
           console.log('sent index.html');
       }
    });
});
